<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFollowUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('follow_users', function (Blueprint $table) {
            $table->string('user');
            $table->string('follow');
            $table->primary(['user', 'follow']);
            $table->timestamps();
        });

        Schema::table('follow_users', function (Blueprint $table) {
            $table->foreign('user')->references('nick')->on('users');
            $table->foreign('follow')->references('nick')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('follow_users', function (Blueprint $table) {
            $table->dropForeign('follow_users_user_foreign');
            $table->dropForeign('follow_users_follow_foreign');
        });
        Schema::dropIfExists('follow_users');
    }
}
