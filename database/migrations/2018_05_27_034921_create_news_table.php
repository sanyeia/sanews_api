<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_nick');
            $table->string('photo')->nullable();
            $table->string('title');
            $table->text('desc', 1000);
            $table->integer('likes')->default(0);
            $table->text('location');

            $table->timestamps();
        });

        Schema::table('news', function (Blueprint $table) {
            $table->foreign('user_nick')->references('nick')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news', function (Blueprint $table) {
            $table->dropForeign('news_user_nick_foreign');
        });
        Schema::dropIfExists('news');
    }
}
