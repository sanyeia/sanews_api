<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PhotoController extends Controller
{
    public function show(Photo $photo)
    {
        return $this->showOne($photo->url);
    }
}
