<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Usuario;
use App\Photo;

class UsuarioController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('store');
    }

    public function store(Request $request) {
        if($request->has('user')){
            $user = new Usuario();
            $user->nick = $request->user['nick'];
            $user->fill($request->user);
            $user->save();

            if($request->has('image')){
                $photo = new Photo();
                $photo->element_id = $user->nick;

                $imageResult = $this->imageStorage(Uuid::generate(4)->string, $request->image);

                $photo->path = $imageResult['path'];
                $photo->id = $imageResult['uuid'];
                $photo->url = $imageResult['url'];
                $photo->active = true;
                $photo->save();

                $user->foto = $photo->url;
            }

            $user->save();
            return response($user,201);
        }else{
            return response('Bad Request', 400);
        }
    }

    public function show(Usuario $user) {
        $result = [
            'Usuario:' => $user,
            'Seguidores' => $user->followers->count(),
            'Siguiendo' => $user->following->count(),
            'Noticias' => $user->noticias,
        ];

        return response($result);
    }

    public function update(Request $request, Usuario $user) {
        if($request->has('user')){
            $user->fill($request->user);
            $user->save();
            return response($user);
        }else{
            return response('Bad Request', 400);
        }
    }

    public function destroy(Usuario $user) {
        $user->noticias()->delete();
        $user->delete();
        return response($user);
    }

    public function check($user) {
        $result = Usuario::where('nick', 'LIKE', '%'.$user.'%')
            ->orWhere('email', 'LIKE', '%'.$user.'%')
            ->exists();
        if($result){
            return response('Existe',200);
        }
        return response('Nuevo',404);
    }

    public function active(Request $request) {
        $result = $request->user();
        if($result){
            return response($result,200);
        }else{
            return response('Unauthenticated',401);
        }
    }
}
