<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;
use App\NewPost;
use Illuminate\Database\Eloquent\Collection;
use App\Traits\ApiResponser;
use ArrayObject;
use App\Photo;
use Webpatser\Uuid\Uuid;

class NewPostController extends Controller
{
    /**
     * lista todas las recetas.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $recetas = NewPost::orderBy('created_at','desc')->with('usuario')->paginate(5);
        return response($recetas);
    }

    /**
     * crea una nueva receta.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if($request->has('noticia')){
            $noticia = new NewPost();
            $noticia->user_nick = request()->user()->nick;
            $noticia->fill($request->noticia);
            $noticia->save();

            if($request->has('file')){
                $photo = new Photo();
                $photo->element_id = request()->user()->nick;

                $imageResult = $this->fileStorage(Uuid::generate(4)->string, $request->file);

                $photo->path = $imageResult['path'];
                $photo->id = $imageResult['uuid'];
                $photo->url = $imageResult['url'];
                $photo->active = true;
                $photo->save();

                $noticia->photo = $photo->url;
                $noticia->save();
            }
            return response($noticia,201);
        } else{
            return response('Bad Request', 400);
        }
    }

    /**
     * muestra una receta en especifico. (y por ahora sus comentarios tambien)
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\NewPost  $noticia
     * @return \Illuminate\Http\Response
     */
    public function show(NewPost $noticia) {
        $result = [
            'usuario' => $noticia->usuario,
            'noticia' => $noticia,
        ];
        return response($result);
    }

    /**
     * busca una receta por nombre
     *
     * @param  $noticia (cadena con el nombre de la receta)
     * @return \Illuminate\Http\Response
     */
    public function searchRecipe($noticia) {
        $RecipeList =  NewPost::where('title', 'LIKE', '%'.$noticia.'%')->get();
		return response($RecipeList);
    }

    /**
     * actualiza la informacion de una receta receta.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  App\NewPost  $noticia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewPost $noticia) {
        if($request->has('noticia')){
            $noticia->fill($request->noticia);
            $noticia->save();

            return response($noticia);
        } else{
            return response('Bad Request', 400);
        }
    }

    /**
     * elimina una receta y todo lo relacionado con ella.
     *
     * @param  App\NewPost  $noticia
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewPost $noticia) {
        $noticia->delete();
        return response($noticia);
    }

    /**
     * recetas de un usuario
     *
     * @param  App\IngredientesNewPost  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function userRecepies(Usuario $user) {
        $result = [];
        foreach ($user->noticias as $key => $noticia) {
            $result['noticia '.$key] = [
                'usuario' => $noticia->usuario,
                'noticia' => $noticia,
            ];
        }
        $result = new Collection($result);
        return response($result);
    }

    /**
     * recetas de usuarios que sigo
     *
     * @param  App\IngredientesNewPost  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function followingUsersRecepies() {
        $user = request()->user();
        $result = [];

        $key = 1;
        foreach ($user->noticias as $noticia) {
            $noticia->ingredientes;
            $result['noticia '.$key] = [
                'usuario' => $noticia->usuario,
                'noticia' => $noticia,
            ];
            $key++;
        }

        foreach ($user->following as $key => $userFollow) {
            foreach ($userFollow->noticias as $key => $noticia) {
                $result['noticia '.$key] = [
                    'usuario' => $noticia->usuario,
                    'noticia' => $noticia,
                ];

            }
            $key++;
        }

        $result = new Collection($result);
        return response($result);
    }
}
