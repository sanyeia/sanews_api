<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Usuario;

class UsuariosSeguidosController extends Controller
{
    /**
     * Seguir a un usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function followUser()
    {
        $followU = Usuario::find(request()->followU);
        if(!isset($followU)){
            return response('Operacion invalida', 400);
        }
        $user = request()->user();
        if ($user->nick != $followU->nick) {
            $user->follow($followU);
            return response( (string)$user->nick . ' ahora sigue a '. (string)$followU->nick, 200);
        } else {
            return response('Operacion invalida', 400);
        }
    }

    /**
     * Dejar de seguir a un usuario
     *
     * @return \Illuminate\Http\Response
     */
    public function unfollow()
    {
        $followU = Usuario::find(request()->followU);
        if(!isset($followU)){
            return response('Operacion invalida', 400);
        }
        if ($user->nick != $followU->nick) {
            $user->unfollow($followU);
            return response($user->nick . ' dejo de seguir a '. $followU->nick, 200);
        } else {
            return response('Operacion invalida', 400);
        }
    }
}
