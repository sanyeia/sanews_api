<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Webpatser\Uuid\Uuid;
use Illuminate\Support\Facades\Storage;
use App\Photo;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function fileStorage($fileid, $file){
        $extension = strtolower($file->getClientOriginalExtension());
        $path = 'public/files/' . $fileid . '.' . $extension;
        $url = asset('files/' . $fileid . '.' . $extension);
        Storage::disk('local')->put($path, \File::get($file));
        return ['path'=>$path,'uuid'=>$fileid, 'url'=>$url];
    }

    public function removeFile($model){
        if($model->photo != null and $model->photo != ""){

            $file = Photo::where('url', $model->photo)->first();
            if (unlink(storage_path("app/$file->path"))) {
                $file->forceDelete();
            } else {
                return null;
            }
            return true;

        } else {
            return false;
        }
    }
}
