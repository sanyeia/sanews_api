<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewPost extends Model
{
    protected $table = 'news';
    protected $fillable = [
        'title',
        'desc',
        'location'
    ];

    public function usuario(){
        return $this->belongsTo('App\Usuario', 'user_nick');
    }
}
