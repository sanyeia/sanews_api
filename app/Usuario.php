<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;


class Usuario extends Authenticatable
{
    use HasApiTokens;

    protected $table = 'users';
    protected $primaryKey = 'nick';
    public $incrementing = false;
    protected $fillable = [
        'password',
        'first_name',
        'last_name',
        'photo',
        'email',
        'birthdate',
        'identification',
    ];

    protected $hidden = ['password'];

    public function setPasswordAttribute($value){
      if (!empty($value)) $this->attributes['password'] = \Hash::make($value);
    }

    public function findForPassport($username) {
        return $this->where('nick', $username)->first();
    }

    public function getRouteKeyName(){
        return 'nick';
    }

    //relaciones
    public function noticias(){
        return $this->hasMany('App\NewPost', 'user_nick', 'nick');
    }

    public function following(){
        return $this->belongsToMany('App\Usuario', 'follow_users', 'user', 'follow')->select('nick');
    }

    public function followers(){
        return $this->belongsToMany('App\Usuario', 'follow_users', 'follow', 'user')->select('nick');
    }

    public function follow(Usuario $user) {
        $this->following()->attach($user->nick);
    }

    public function unfollow(Usuario $user) {
        $this->following()->detach($user->nick);
    }

}
