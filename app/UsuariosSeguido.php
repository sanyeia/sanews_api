<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsuariosSeguido extends Model
{
    protected $table = 'follow_users';
    public $incrementing = false;
}
