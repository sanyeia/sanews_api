<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//auth
// Route::post('oauth/logout','User\UserController@logoutApi');
// Route::post('oauth/login','User\UserController@login');

//usuarios
Route::get('users/active', 'UsuarioController@active');
Route::get('users/{user}/verify', 'UsuarioController@check');
Route::resource('users', 'UsuarioController', ['except'=>['create', 'edit','index']]);

//seguir usuarios
Route::post('follow', 'UsuariosSeguidosController@followUser');
Route::delete('unfollow', 'UsuariosSeguidosController@unfollow');

//noticias
Route::get('users/{user}/news', 'NewPostController@userRecepies');
Route::get('follow/news', 'NewPostController@followingUsersRecepies');
Route::resource('news', 'NewPostController', ['except'=>['create', 'edit', 'store']]);
Route::get('news/search/{noticia}', 'NewPostController@searchRecipe');
Route::post('news', 'NewPostController@store');

//imagen
Route::get('photos/{photo}', 'PhotoController@show'); // Obtener una imagen

